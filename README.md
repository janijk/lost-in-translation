# Lost in Translation
## Table of contents
* [Introduction](#introduction)
* [Features](#features)
* [Technologies](#technologies)
* [Usage](#usage)
* [Authors](#authors)
* [Sources](#sources)

## Introduction
Translation app: English to American sign language. One page web app with three different views: startup is where the login or account creation happens, translation is where you translate text to sign language and profile for viewing history. User data is stored via 3rd party API. React router is used for navigation and Redux toolkit for state management.

## Features
- Translate text or sentences to sign language
- Authentication / authorization
- Store users translation history 

## Technologies
- JavaScript
- React
- Redux
- Heroku
- HTML/CSS
- REST API

## Usage
Heroku deployed app [Here](https://intense-atoll-67742.herokuapp.com/)

## Authors
[@Emil](https://gitlab.com/emilcalonius)<br />
[@Jani](https://gitlab.com/janijk)

## Sources
Project was an assignment done during education program created by
Noroff Education