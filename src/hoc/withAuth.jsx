import { Navigate } from "react-router-dom"
import { useSelector} from 'react-redux';

const withAuth = Component => props => {
    const currentUser = useSelector(state => state.username.value);
    if(currentUser !== ""){
        return <Component {...props}/>
    }
    else return <Navigate to="/"/>
};
export default withAuth;