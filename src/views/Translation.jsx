import React from 'react';
import { useState } from 'react';
import Header from '../components/Header';
import TextPrompt from '../components/TextPrompt';
import TranslationBox from '../components/TranslationBox';
import withAuth from '../hoc/withAuth';
import { useSelector, useDispatch } from 'react-redux';
import { addTranslation } from '../api/translation';


const Translation = () => {
    const inputPlaceholder = "Word or sentence to translate";
    const [textToTranslate, setTextToTranslate] = useState("");
    const [letters, setLetters] = useState([]);

    const currentUser = useSelector(state => state.username.value);
    const dispatch = useDispatch(); 

    const handleClickTextPrompt = async () => {
        // Create array that contains only normal letters
        const arr = textToTranslate.toLowerCase().match(/[a-z]/g);

        // Check that text to translate contains letters
        if(arr.length > 0) {
            setLetters(arr);

            // Add translation to user's history
            await addTranslation(currentUser, textToTranslate);
        }
    };

    const handleOnChange = (event) => {
        setTextToTranslate(event.target.value)
    };

    return (
        <div>
            <Header 
                enableLogo={true}
                enableAvatar={true}
                username={currentUser}
            />
            <div>
                <TextPrompt
                    value={textToTranslate}
                    handleOnChange={handleOnChange}
                    handleOnClick={handleClickTextPrompt}
                    inputPlaceholder={inputPlaceholder}
                />
                <TranslationBox
                    sentence={letters}
                />
            </div>
            
        </div>
    )
}
export default withAuth(Translation); 