import React from 'react';
import { useState } from 'react';
import Header from '../components/Header';
import TextPrompt from '../components/TextPrompt';
import robotHello from "../assets/Logo-Hello.png";
import './Startup.css';
import PasswordInput from '../components/PasswordInput';
import { useSelector, useDispatch } from 'react-redux';
import { setName } from "../reduxparts/userSlicer";
import { loginUser, createUser } from '../api/user.js';
import bcrypt from 'bcryptjs';

const Startup = () => {
    const [showPasswordInput, setshowPasswordInput] = useState(false);
    const [existingUser, setExistingUser] = useState(true);
    const [username, setUsername] = useState("");
    const [passwordOne, setPasswordOne] = useState("");
    const [passwordTwo, setPasswordTwo] = useState("");
    const [message, setMessage] = useState("");
    
    const currentUser = useSelector(state => state.username.value);
    const dispatch = useDispatch();    

    // When clicking arrow button check if user exists in database
    // If not => proceed as new user, if exists => proceed as existing user
    const handleClickTextPrompt = async () => {
        if(username !== ""){          
            const [error,user] = await loginUser(username);
            console.log("Errors: " + error);            
            if(user.length !== 0){
                setExistingUser(false);           
            }            
            setshowPasswordInput(!showPasswordInput); 
        }              
    };

    // After password query => check if it is existing or new user.
    // If new => create user with given password, if existing => check that password is correct
    const handleClickPassword = async () => {        
        if (passwordTwo !=="" && passwordOne === passwordTwo) {
            const salt = bcrypt.genSaltSync(10);
            const hash = bcrypt.hashSync(passwordOne, salt);
            const [error,user] = await createUser(username, hash);
            console.log("Errors: " +error);
            if(user.username) dispatch(setName(user.username));
        }else{
            const [error,user] = await loginUser(username);
            const match = bcrypt.compareSync(passwordOne, user.password);
            console.log("Errors: " +error);
            if(match) dispatch(setName(user.username));
            else {
                setMessage("Wrong password");
                setPasswordOne("");
            }
        }
    };

    return (  
        <div className='outerStartupWrap'>
            <Header 
                enableLogo={false}
                enableAvatar={false}
                username={currentUser}
            />
            <div className='startupContentWrap'>
                <div className="startupLogoWrap">
                    <img className='startupLogoHello' src={robotHello} alt="robo" />
                </div>
                <div className='startupHeaders'>
                    <h1 className='startupHeadOne'>Lost in translation</h1>
                    <h3 className='startupHeadThree'>Get started</h3> 
                </div>                         
            </div>
            <div className='startupTextInputBg'>
                <div className='startupTextInput'>
                    {!showPasswordInput ?
                        <TextPrompt
                            value={username}
                            handleOnChange={e => setUsername(e.target.value)}
                            handleOnClick={handleClickTextPrompt}
                            inputPlaceholder={"Username"}
                        />
                        :
                        <>
                            <PasswordInput
                                dualInput={existingUser}
                                username={username}
                                handleOnChangeFirst={e => setPasswordOne(e.target.value)}
                                handleOnChangeSecond={e => setPasswordTwo(e.target.value)}
                                handleOnClick={handleClickPassword}
                            />  
                            <div className='startupMessageText'>{message}</div>
                        </>                     
                    }                    
                </div>                
            </div> 
        </div>              
    )
}

export default Startup