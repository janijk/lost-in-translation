import React, { useState, useEffect } from 'react';
import Header from '../components/Header';
import TranslationHistory from '../components/TranslationHistory';
import './Profile.css';
import withAuth from '../hoc/withAuth';
import { useSelector, useDispatch } from 'react-redux';

const Profile = () => {
    const logoForHeader = true;
    const enableAvatar = true;
    const currentUser = useSelector(state => state.username.value);
    const dispatch = useDispatch();

    return (
        <div className='profile'>
            <Header 
                enableLogo={logoForHeader}
                enableAvatar={enableAvatar}
                username={currentUser}
            />
            <div className='header'>Translation history</div>
            <TranslationHistory
                username={currentUser}
            />
        </div>
    )
}
export default withAuth(Profile)