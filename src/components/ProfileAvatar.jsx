import React from "react";
import "./ProfileAvatar.css"
import NavBar from "./NavBar";

/**
 * Profile component which shows username, picture and logout button.
 * 
 * @param {*} username name of the logged in user 
 * @param {*} img picture of the user, defaults to robot 
 * @param {*} handleClickLogout logout button onClick
 * @returns div which has p, image and button as children
 */
const ProfileAvatar = (props) => {

    return (
        <div className="outerProfileAvatarWrap">
            <div className="profileAvatarUsernameWrap">
                <p className="profileAvatarUsername">{props.username}</p>
            </div>
            <div className="profileAvatarWrap">
                <img className="profileAvatarImg" src={props.img} alt={props.img}/>
            </div>
            <div className="profileAvatarLogoutWrap">
                <NavBar />
            </div>
        </div>        
    )
}
export default ProfileAvatar