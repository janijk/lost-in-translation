import React, { useEffect, useState } from "react";
import "./TranslationBox.css";

/**
 * Component that translates a sentence into 
 * sign language and displays the result.
 * 
 * @param {*} sentence sentence that needds to be translated 
 * @returns The div that contains the translated sentence
 */
const TranslationBox = (props) => {
    return(
        <div className="translation-box">
            <div className="translation">
                {props.sentence != null ?
                    props.sentence.map((letter, index) => <img className="sign" key={index} src={`individual_signs/${letter}.png`} />)
                    :
                    <></>
                }
            </div>
            <div className="footer">
                <div className="text">Translation</div>
            </div>
        </div>
    );
}

export default TranslationBox;