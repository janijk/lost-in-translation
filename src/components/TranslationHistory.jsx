import React, { useEffect, useState } from "react";
import "./TranslationHistory.css";
import { checkForUser } from "../api/user";
import { clearTranslationHistory } from "../api/translation";

/**
 * Component that displays previously made translations for user.
 * 
 * @returns The div that contains translation history
 */
const TranslationBox = (props) => {
    const [translations, setTranslations] = useState([]);

    const getTranslations = async () => {
        const [error, user] = await checkForUser(props.username);
        setTranslations(user[0].translations);
        console.log(user[0].translations);
    }

    useEffect(() => {
        getTranslations();
    }, []);

    return(
        <div className="translation-history-container">
            <div className="translation-history">
                {
                    translations.map((translation, index) => {
                        return(<div key={index} className="single-translation">
                            {translation}
                        </div>)
                    })
                }
            </div>
            <div className="footer">
                <button 
                    className="clear-btn" 
                    onClick={async () => {
                        await clearTranslationHistory(props.username);
                        getTranslations();
                    }}
                >Clear history</button>
            </div>
        </div>
    );
}

export default TranslationBox;