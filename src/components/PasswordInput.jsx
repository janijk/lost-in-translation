import React from 'react';
import './PasswordInput.css';
/**
 * Returns one or two inputs and a button depending on dualInput value, can be used as password entry
 * component for existing or new users.
 * 
 * @param {*} dualInput true/false for one or two password input fields
 * @param {*} username
 * @param {*} handleOnClick onClick button
 * @param {*} handleOnChangeFirst onChange of input 1
 * @param {*} handleOnChangeSecond onChange of input 2
 * @returns 
 */
const PasswordInput = (props) => {

    return (
        <div className='passwordInputOuterWrap'>
            {props.dualInput ?
                (                    
                    <div className='passwordInputContentWrap'>
                        <div className='passwordInputTextField'>
                            <h4 className='passwordInputHeaderFour'>
                                {`Create password for new user: ${props.username}`}
                            </h4>
                        </div>
                        <input 
                            onChange={props.handleOnChangeFirst} 
                            className='passwordInputInput' 
                            type="password" 
                            placeholder='password'
                        />
                        <input 
                            onChange={props.handleOnChangeSecond} 
                            className='passwordInputInput' 
                            type="password" 
                            placeholder='password (again)'
                        />                        
                        <button onClick={props.handleOnClick} className='passwordInputButton'>
                            Create account
                        </button>                        
                    </div>
                ) : (
                    <div className='passwordInputContentWrap'>
                        <div className='passwordInputTextField'>
                            <h4 className='passwordInputHeaderFour'>{`Login as: ${props.username}`}</h4>
                        </div>
                        <input 
                            onChange={props.handleOnChangeFirst} 
                            className='passwordInputInput' 
                            type="password" 
                            placeholder='password'
                        />                        
                        <button onClick={props.handleOnClick} className='passwordInputButton'>
                            Login
                        </button>                        
                    </div>
                )
            }
        </div>
    )
}
export default PasswordInput