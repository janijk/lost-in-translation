import { NavLink } from "react-router-dom";
import "./NavBar.css";
import { useDispatch } from "react-redux";
import { setName } from "../reduxparts/userSlicer";

/**
 * Navbar which return link to translation and profile page
 * as well as logout button
 * @returns nav -> ul -> li's -> NavLink/Button
 */
const NavBar = () => {
  // Styling for NavLinks to indicate which page is the user on
  const astyling = ({ isActive }) =>
    isActive
      ? {
          color: "#845EC2",
          textDecoration: "none",
          liststyle: "circle",
          fontWeight: 700,
        }
      : {
          textDecoration: "none",
          color: "white",
          fontWeight: 700,
        };

  const dispatch = useDispatch();

  const handleOnClickLogout = () => {
    dispatch(setName(""));
  };

  return (
    <nav>
      <ul className="NavBarUl">
        <li className="NavBarLi">
          <NavLink to="/Profile" style={astyling}>
            Profile
          </NavLink>
        </li>
        <li className="NavBarLi">
          <NavLink to="/Translation" style={astyling}>
            Translation
          </NavLink>
        </li>
        <li className="NavBarLi">
          <button onClick={handleOnClickLogout} className="NavBarButton">
            Logout
          </button>
        </li>
      </ul>
    </nav>
  );
};
export default NavBar;
