import React from "react"
import ProfileAvatar from "./ProfileAvatar"
import robotLogo from "../assets/Logo.png"
import './Header.css'

/**
 * Page header component which has logo, Lost in translation h1 text and user profile.
 * enabling logo and user profile is optional
 * 
 * @param {*} enableLogo    enable robot logo: true/false
 * @param {*} enableAvatar  enable avatar: true/fals
 * @param {*} username  name of the logged in user
 * @param {*} avatarImg image of user, defaults to robot
 * @returns div containing image, h1 and Header children
 */
const Header = (props) => {
    const isLogo = props.enableLogo ? robotLogo : null;
    const isAvatar = props.enableAvatar ? true : false;    
    const avatarImg = props.avatarImg ? props.avatarImg : robotLogo;
    const userName = props.username;
    const headerOne = "Lost in translation";

    const handleClickLogout = () => {
        console.log("Logout button pressed");
        // logout code here
    };

    return (
        <div className="headerWrap">
            <div className="headerLogoWrap">
               <img className="headerRobotLogo" src={isLogo}/> 
            </div>            
            <h1 className="headerLiT">{headerOne}</h1> 
                {
                    isAvatar ? (
                        <ProfileAvatar 
                            username={userName} 
                            img={avatarImg} 
                            handleOnClick={handleClickLogout}
                        />
                    ) : (
                        <div style={{flex:4}}></div>   
                    )
                }          
        </div>
    )
}

export default Header