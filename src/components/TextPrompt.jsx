import React from "react"
import keyboard from "../assets/keyboard.png"
import arrow from "../assets/arrow-right-circle.svg"
import "./TextPrompt.css"

/**
 * Text input component, has keyboad icon on the left side of text input and circular arrow button on the right side.
 * 
 * @param {*} inputPlaceholder text for input placeholder
 * @param {*} handleOnClick button onClick
 * @param {*} handleOnChange input onChange
 * @returns div which has img, input and button as children.
 */
const TextPrompt = (props) => {  

    return (
        <div className="textPromptOuterWrap">
            <img className="textPromptKeyboardImg" src={keyboard} alt="keyboard"/>            
            <input 
                type="text"
                className="textPromptTextInput"
                onChange={props.handleOnChange}                 
                placeholder={props.inputPlaceholder}
            />
            <button className="textPromptCircleButton" onClick={props.handleOnClick}>
                <img className="textPromptArrowImg" src={arrow} alt="arrow"/>
            </button>
        </div>
    )
}

export default TextPrompt