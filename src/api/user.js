import { createHeaders } from "./index.js";

const apiUrl = process.env.REACT_APP_API_URL;

/**
 * Check if user exists in database. Returns [null, object] if exists
 * else [errorMsg, []]
 * @param {*} username 
 * @returns array [errorMsg, user]
 */
export const checkForUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}?username=${username}`);
        if(!response.ok){
            throw new Error('could not find user')
        }
        const data = await response.json();
        return[null,data];
    } catch (error) {
        return [error.message, []];
    }
};

/**
 * Create user with username and password parameters. Returns [null, object] if success
 * else [errorMsg, []]
 * 
 * @param {*} username 
 * @param {*} password 
 * @returns array
 */
export const createUser = async (username, password) =>{
    try {
        const response = await fetch(apiUrl, {
            method:'POST',
            headers:createHeaders(),
            body: JSON.stringify({
                username,
                password,
                translations: []
            })});
        if(!response.ok){
            throw new Error('could not create user')
        }
        const data = await response.json();
        return[null,data];
    } catch (error) {
        return [error.message, []];
    }
};

/**
 * Try to login with username param. Returns [null, object] if success,
 * [errorMsg, []] if fail
 * @param {*} username 
 * @returns array
 */
export const loginUser = async (username) => {
    const [checkError, user] = await checkForUser(username);

    if (checkError != null) {
        return [checkError, null]
    }
    if(user.length>0){
        return [null, user.pop()]
    }
    return [checkError, user];   
};