const apiKey = process.env.REACT_APP_API_KEY

// Header for POST methods
export const createHeaders = () => {
    return {
        'Content-Type':'application/json',
        'x-api-key':apiKey
    }
}