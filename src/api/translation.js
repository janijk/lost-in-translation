import { createHeaders } from "./index.js";
import { checkForUser } from "./user.js";

const apiUrl = process.env.REACT_APP_API_URL;

/**
 * Add a translation to a user object.
 * 
 * @param {*} username user to be updated
 * @param {*} translation information to add to user
 * @returns [null, object] if success, else [errorMsg, []]
 */
export const addTranslation = async (username, translation) => {
    try {
        const [error, user] = await checkForUser(username); 
        const response = await fetch(`${apiUrl}/${user[0].id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user[0].translations, translation]
            })
        });

        if(!response.ok) {
            throw new Error('Could not update translations');
        }

        const result = await response.json();
        return [ null, result ];
    } catch(error) {
        return [ error.message, null ];
    }
}

/**
 * Clear a user's translation history.
 * 
 * @param {*} username user to be updated
 * @returns [null, object] if success, else [errorMsg, []]
 */
export const clearTranslationHistory = async (username) => {
    try {
        const [error, user] = await checkForUser(username); 
        const response = await fetch(`${apiUrl}/${user[0].id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        });

        if(!response.ok) {
            throw new Error('Could not update translations');
        }

        const result = await response.json();
        return [ null, result ];
    } catch(error) {
        return [ error.message, null ];
    }
}