import { createSlice } from '@reduxjs/toolkit'

export const translationSlice = createSlice({
    name: 'translations',
    initialState: {
      value: []
    },
    reducers: {
      addTranslation: (state, action) => {
        state.value.push(action.payload)
      },
      setTranslations:(state, action) => {
        state.value = action.payload
      }
    }
  })  
  export const {addTranslation, setTranslations } = translationSlice.actions
  export default translationSlice.reducer