import { configureStore, combineReducers } from '@reduxjs/toolkit'
import storageSession from 'redux-persist/lib/storage/session'
import userSlicer from './reduxparts/userSlicer'
import translationSlicer from './reduxparts/translationSlicer'
//import storage from 'redux-persist/lib/storage';
import { persistReducer, persistStore } from 'redux-persist';
import thunk from 'redux-thunk';

const persistConfig = {
    key: 'root',
    storage: storageSession,
}

const rootReducer = combineReducers({ 
    username: userSlicer,
    translations: translationSlicer,
  })

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
    reducer: persistedReducer,
    middleware: [thunk]
})

export const persistor = persistStore(store)