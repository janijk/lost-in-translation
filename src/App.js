import React from 'react';
import './App.css';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import Startup from './views/Startup';
import Profile from './views/Profile';
import Translation from './views/Translation';
import { useSelector} from 'react-redux';

function App() {
  const isAuth = useSelector(state => state.username.value);
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route path='/' element={isAuth? <Profile/> : <Startup/>}></Route>
          <Route path='/Translation' element={<Translation/>}></Route>
          <Route path='/Profile' element={<Profile/>}></Route>
        </Routes>
      </div>
    </BrowserRouter>    
  );
}

export default App;